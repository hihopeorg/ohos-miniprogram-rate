/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import prompt from '@system.prompt';
export default {
    data: {
        title: 'World',
        value:"5"
    },
    rateChange(e){
        prompt.showToast({
            message:`${e.detail.value}星`
        })
    },
    rateChangeOne(e){
        this.value = e.detail.value;
        prompt.showToast({
            message:`${e.detail.value}星`
        })
    }
}

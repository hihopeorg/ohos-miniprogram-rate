# ohos-miniprogram-rate

## 简介
ohos-miniprogram-rate是一实现星星评分功能库。

## 效果展示：
![png](preview.gif)
## 安装教程

1. 参考安装教程 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md)
2. 安装命令如下：
 ```
 npm install @ohos/miniprogram_rate --save
 ```

#### 使用说明：

第一步： 在hml文件中引入组件；
```html
<element name="hw-rate" src="../../../default/miniprogram-rate/miniprogram_dev/index.hml"></element>
```

第二步：创建组件节点：
```html
    <hw-rate value="{{2.5}}" allow-half="{{true}}" length="{{5}}" disabled="{{false}}" @change="rateChange"></hw-rate>
```

 - 注意:导入请求入口js文件时需要根据自己的项目路径;具体详细使用方法可查看项目的案例。
   

##### Props

| name     | description              | type     | default value |
| :---------------- | :----------------------- | :------  | :------------ |
| value            |  选中的星星数                | Number    | 0        |
| placeholder             | 星星占位符                 | Number    | 0          |
| allowHalf             | 是否可半星                 | Boolean    | true          |
| disabled             | 是否禁用选择                 | Boolean    | false       |
| length             | 星星个数                 | Number    | 5       |

##### events

| name     | description              | parameters     |
| :---------------- | :----------------------- | :------  |
| change            |  改变了星星                | 改变的数值    |

## 接口说明
1. 根据传入的值刷新星星
   `_refreshValues(newV);`
2. 根据传入的值刷新占位符
   `_refreshPlaceholders(newV)`
3. 生成最终要显示的星星数量数组
   `_generateArray(value)`
4. 点击选中星星
   `choose(e)`
5. 设置值
   `setVal(v)`
6. 判断是否为整数
   `isPositiveIntegerNumber(num)`

## 兼容性

支持OpenHarmony  API version 8 及以上版本

## 目录结构
````
|---- ohos-miniprogram-rate
|     |---- entry  # 示例代码文件夹
|     |---- miniprogram-rate  # miniprogram-rate库文件夹
│           ├----common # 框架代码目录
│               ├----miniprogram_dev # 框架代码目录
│                 ├----index.html # 评分ui库
│                 
│                  
└─resources # 资源文件
|     |---- README.MD  # 安装使用方法  
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/hihopeorg/ohos-miniprogram-rate/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/hihopeorg/ohos-miniprogram-rate/pulls) 。

## 开源协议
本项目基于 [MIT License](https://gitee.com/hihopeorg/ohos-miniprogram-rate/blob/master/LICENSE) ，请自由地享受和参与开源。

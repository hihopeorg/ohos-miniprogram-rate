/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export default {
    onInit() {
        this.$watch('value', '_refreshValues');
        this.$watch('placeholder', '_refreshPlaceholders');
        if (this.value === 0) {
            this._refreshPlaceholders(this.placeholder)
        }else if(this.value !==0 ){
            this._refreshValues(this.value)
        }
    },
    props: {
        value: {
            default: 0,
        },
        placeholder: {
            default: 0,
        },
        allowHalf: {
            default: true
        },
        disabled: {
            default: false
        },
        length: {
            default: 5
        },
    },
    data: {
        iconClassMap: {
            '0': 'empty',
            '0.5': 'half',
            '1': 'full'
        },
        iconText: 'star',
        values: [],
        placeholders: [],
    },
    onReady(){
    },
    _refreshValues(newV) {
//        console.log(`_refreshValues new:${newV} old:${oldV}`);
        this.values = this._generateArray(Number(newV))
    },
    _refreshPlaceholders(newV) {
        this.placeholders = this._generateArray(Number(newV))
    },
    _generateArray(value) {
        let arr = []

        let fullCount = value
        if (fullCount <= 0) {
            return [0, 0, 0, 0, 0]
        }

        let isAppendHalf = false // 是否需要加半星
        if (this.allowHalf) {
            // 允许半星的向下取整
            if (!isPositiveIntegerNumber(fullCount)) {
                // 非整数
                fullCount = Math.floor(fullCount)
                isAppendHalf = true
            }
        } else {
            // 不允许半星的向上取整
            fullCount = Math.ceil(fullCount)
        }
        // 循环追加完整分数
        for (let i = 0; i < fullCount; i++) {
            arr.push(1)
        }
        if (isAppendHalf) {
            arr.push(0.5)
        }
        // 循环追加空分
        let emptyCount = Number(this.length) - arr.length
        for (let i = 0; i < emptyCount; i++) {
            arr.push(0)
        }

        return arr
    },
    choose(e) {
        if (this.disabled) {
            return
        }

        let index = Number(e.target.dataSet.index)
        let isHalf = Number(e.target.dataSet.half) === 1
        let value = (index + 1) * 2
        if (this.allowHalf) {
            // 允许半个
            if (isHalf) {
                // 点了左一半
                value = value - 1
            }
        }
        value = (value / 2).toFixed(1)

        if (value === this.value) {
            return
        }
        this.value = value
        this.$emit('change', {
            value
        })
    },
    setVal(v){
        this.value = v;
    }
}

// 是否为整数
function isPositiveIntegerNumber(num) {
    let numStr = num.toString()
    return /(^[1-9]\d*$)/.test(numStr)
}